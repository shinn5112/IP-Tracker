#! /usr/bin/python3
"""
author: Patrick Shinn

email: shinn5112@gmail.com

This program is an IP tracker that will email the user with the new IP address anytime the IP changes,
as well as perform house keeping for a NextCloud installation.
"""

from time import sleep
import datetime
import smtplib
import os
import socket
import ipgetter


def get_settings():
    """
    Opens and reads the settings file.

    :return: settings list: str list
    """
    settings_list = []
    settings_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), "settings.txt")
    settings_file = settings_file.strip('\n\t')
    status_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), "status.txt")
    status_file = status_file.strip('\n\t')
    settings = open(settings_file, 'r')
    for setting in settings:
        if setting.startswith('#'):  # allows for comments using '#' in the status file.
            pass
        else:
            setting = setting.rstrip('\n')
            settings_list.append(setting)
    settings_list.append(status_file)
    return settings_list


def ip_check(ip_type, log_file):
    """
    Checks the IP by looking at the last IP we had against a fresh IP check.
    This will write any changes to the log file.

    :param ip_type: integer value representing the current ip type to track.
    :type ip_type: str
    :param log_file: file used to log errors that occur.
    :type log_file: file
    :return: returns the current ip: str
    """

    done = False
    print("test")
    read = 'hh'  # stores read from loop.
    while not done:  # continues until IP can be procured
        print("going")
        # sends dig command to command line, reads it, then converts it to string removing all new lines, tabs

        if int(ip_type) == 1:  # if we are tracking a local IP
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect(("8.8.8.8", 80))
            read = s.getsockname()[0]

            s.close()
        else:             # if we are tracking a WAN IP
            read = ipgetter.myip()

        read = str(read)
        read = read.strip("\n\t")
        print(read)
        # catches the listed errors and retries or returns the correct WAN IP.
        if read == ';; connection timed out; no servers could be reached':
            log_file.write('WAN Error: ' + str(read) + ' reattempting. \n')
            sleep(5)
            continue
        elif read == ' ;; connection timed out; no servers could be reached':
            log_file.write('WAN Error: ' + str(read) + ' reattempting. \n')
            sleep(5)
            continue
        elif read == '':
            log_file.write('WAN Error: ' + str(read) + ' reattempting. \n')
            sleep(5)
            continue
        elif read == "dig: couldn't get address for 'ns1.google.com': not found":
            log_file.write('WAN Error: ' + str(read) + ' reattempting. \n')
            sleep(5)
            continue
        elif read == ' ':
            log_file.write('WAN Error: ' + str(read) + ' reattempting. \n')
            sleep(5)
            continue
        else:
            break
    return read


def php_config(current_ip, php_location):
    """
    Edits the NextCloud configuration file to accept connections a new IP address
    by editing the config.php file.

    :param current_ip: ip to be written to NextCloud config.php
    :type current_ip: str
    :param php_location: path to NextCloud config.php
    :type php_location: str
    :return: None
    """
    line_number = 0  # starting line number
    line_store = []  # stores lines to write to config.php
    current_ip = current_ip.strip('\n"')
    config = open(php_location, 'r')  # read the current config.php
    for line in config:
        if line_number != 8:
            line_store.append(line)  # if not line 8, copy lines verbatim to line_store for rewriting
        else:
            line_store.append("    1 => '" + current_ip + "'," + '\n')  # replace this line with the new IP address
        line_number += 1
    config.close()
    config_rewrite = open(php_location, 'w+')
    for line in line_store:
        config_rewrite.write(line)
    config_rewrite.close()


def send_mail(log_file, current_ip, sender, recipient, subject, password, server_address, server_port, status_file):
    """
    This function takes the various inputs and emails the new server ip address to the receiving address.
    If the server failed to connect within the limit, a status is written to an external file letting
    the program know that it failed to connect on the last run, so it will try again.

    :param log_file: log file.
    :type log_file: file
    :param current_ip: what the ip to be sent is
    :type current_ip: str
    :param sender: sender email address
    :type sender: str
    :param recipient: receiving email address
    :type recipient: str
    :param subject: subject of email
    :type subject: str
    :param password: password of sender email
    :type password: str
    :param server_address: address of the server
    :type server_address: str
    :param server_port: server port being used
    :type server_port: str
    :param status_file: email status file
    :type status_file: str
    :return: None
    """
    status_file = open(status_file, 'w')
    fail_send = False
    server = smtplib.SMTP(server_address, server_port)  # server to be connected to
    log_file.write('\nThe IP changed to: ' + current_ip + ' on ' + str(datetime.datetime.now()) + '\n')
    msg = 'Your IP Address has changed to: ' + current_ip
    body = '\r\n'.join([
        'To: %s' % recipient,
        'From: %s' % sender,
        'Subject: %s' % subject,
        '',
        msg])

    try:
        server.ehlo()
        server.starttls()
        server.ehlo()
        log_file.write("Successful connection to server. \n")
    except (ConnectionError, ConnectionRefusedError, ConnectionAbortedError, ConnectionResetError) as error:
        log_file.write("Connection to server failed: '" + str(error) + "', exiting.  Will reattempt on next run. \n")
        status_file.write('1')  # if the server connection failed, will try again next run
        fail_send = True  # letting the program know the message didn't send.
    if fail_send is True:
        return 0  # if the server failed to connect, no email is sent and program ends.
    else:
        status_file.write('0')  # if the connection was successful, the program will
        try:  # email the new ip
            server.login(sender, password)
            server.sendmail(sender, recipient, body)
            log_file.write('The message was sent successfully. \n \n')
        except (ConnectionError, ConnectionRefusedError, ConnectionAbortedError, ConnectionResetError,
                smtplib.SMTPAuthenticationError) as error:
            # if the email is not sent, wait for 5 seconds and try again.
            log_file.write("Message send Failure: '" + str(error) + "'. \n")

    # Closing all opened files
    status_file.close()
    log_file.close()
    server.quit()


def status_read(status_file):

    """
     Reads the status and returns a value of 1 or 0.

    :param status_file: path to status file.
    :type status_file: str
    :return: email status
    """
    state = ''
    status = open(status_file, 'r')
    for line in status:
        line = line.strip('\n')
        state = int(line)
    return state


def main():
    """
    Checks the current IP against a recorded IP. If they are different,
    the user is sent an email containing the new IP. If the user has installed
    NextCloud and configured the settings file to edit the config.php, this script
    will also handle that.

    :return: None
    """

    settings_list = get_settings()      # read the settings

    old_address = ""                    # place holder for the old ip

    php_file = settings_list[0]         # path to NextCloud config.php
    old_ip_location = settings_list[2]  # path to old wan file
    log_file = settings_list[1]         # path to log file

    # Email Settings
    recipient = settings_list[3]        # email recipient
    sender = settings_list[4]           # sender email
    subject = settings_list[6]          # email subject
    password = settings_list[5]         # sender email
    email_server = settings_list[7]     # server address
    port_number = settings_list[8]      # server port number
    ip_type = settings_list[9]          # ip tracking type, local or WAN

    # Open files
    status_file = settings_list[len(settings_list) - 1]
    log_file = open(log_file, "w+")
    old_address_file = open(old_ip_location)

    new = ip_check(ip_type, log_file)  # sets the new ip to check against.
    current_state = status_read(status_file)

    for ip in old_address_file:  # This iterates over the old WAN IP and appends it to the old variable.
        ip.strip('\n')
        old_address += ip

    if new != old_address_file or current_state != 0:  # if the ip has changed or something went wrong on the last run
        # Updates the old wan to the current wan
        old_address_file.close()
        old_address_file = open(old_ip_location, 'w')
        old_address_file.write(new)
        old_address_file.close()

        # php_config function call updates NextCloud config.php to accept new wan
        if php_file.lower() == 'none':  # if the user has no php file, pass this part
            pass
        else:  # else try to use the user config to rewrite it.
            try:  # if the file is found, rewrite it
                php_config(new, php_file)
            except FileNotFoundError:  # continue the program and send the email
                log_file.write("php file not found at: " + str(php_file) + " file not updated")

        # Sends new ip to email
        send_mail(log_file, new, sender, recipient, subject, password, email_server, port_number, status_file)
        log_file.close()

    else:  # executes if the wan hasn't changed, do nothing.
        old_address_file.close()
        log_file.close()


if __name__ == "__main__":
    main()
