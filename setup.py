#! /usr/bin/python3
"""
author: Patrick Shinn

email: shinn5112@gmail.com

This is the setting setup for ip_tracker.py, run this before running ip_tracker.py
This program will ask a series of ten questions to the user to get the proper settings.
"""
import os


def main():
    """
    Generates a settings file based on user provided information.
    The script also has the ability to edit an already existing settings file.

    :return: None
    """
    done = False  # Boolean for loops  # Settings file to be written to
    old_settings = False  # initial value for old settings test
    settings_list = []  # Stores setting values to be written
    previous_settings = ''  # will be used to check for old settings
    setting_write = False  # checks to see if the setting were written before closing the program
    mode = ''  # first run mode question storage
    welcome_text = 'This program will configure the settings for ip_tracker.py, to ensure that \n' \
                   'the settings are correct, please be sure to enter the correct information. Spelling counts.\n' \
                   'If you are having issues or have questions, please refer to the README.txt. \n'
    pwd = os.getcwd()  # gets current
    pwd = pwd.strip('"\'\n\t')

    log_file = ''
    ip = ''
    server_address = ''
    server_port = ''
    status = ''

    # checks for an old settings file to read
    try:
        previous_settings = open('settings.txt', 'r')
        old_settings = True  # indicates that a settings file exists
    except FileNotFoundError:
        pass

    if old_settings is True:  # if old settings are found, read and imported
        for setting in previous_settings:
            setting = setting.strip()
            settings_list.append(setting)

    print(welcome_text)
    while not done:
        if old_settings is False:  # lets the user know that no previous settings were found
            print("No previous settings were found, please select a configuration mode.\n")
            print("1. Manual (Not recommended for novice programmers).\n"
                  "2. Easy Setup (recommended). \n")
            mode = input("Enter your selection number: ")
        else:  # If a settings file is found, setup is opened in manual config mode.
            print("A settings file was found, you can either edit the file or start fresh by deleting settings.txt. \n")
            mode = '1'
        if int(mode) == 1:
            break
        elif int(mode) == 2:
            break
        else:
            print("'" + mode + "' was not an option, please try again.")

    while not done:
        if mode == '2':  # Easy mode
            while mode == '2':  # while in easy mode, this code executes
                # User will answer the following questions, and the script will try to automate as much as it can.
                q1 = input("Does your server run NextCloud? y/N: ")
                if q1.lower() == 'y':
                    q1 = input("What is the absolute path to your config.php file?: ")
                    php_config = q1
                else:
                    php_config = 'none'
                q2 = input("What email would you like address changes to be sent to?: ")
                q3 = input("What email would you like to send the alerts from?: ")
                q4 = input("What is the sender email's password?: ")
                q7 = input("What would you like the email's subject to be?: ")
                q5 = input("Is the sender email a gmail or a yahoo account? If neither, say neither: ")
                email_confirm = False
                q6 = int(input("Would you like your:\n"
                               "1. local IP\n"
                               "2: WAN IP?\n"
                               ": "))
                while not email_confirm:  # preset email server settings.
                    if q5.lower() == 'gmail':
                        server_address = 'smtp.gmail.com'
                        server_port = '587'
                        email_confirm = True
                    elif q5.lower() == 'yahoo':
                        server_address = 'smtp.mail.yahoo.com'
                        server_port = '587'
                        email_confirm = True
                    elif q5.lower() == 'neither':
                        mode = '1'
                        print("You will need to manually configure your settings, please select option 1"
                              " to configure settings.")
                        break
                    else:
                        print('"' + q5 + '"' + ' was not an option, please try again.')
                        q5 = input("Is the sender email a gmail or a yahoo account? If neither, say neither: ")
                if mode == '1':
                    break

                ip = pwd + '/ip.txt'
                log_file = pwd + '/log.txt'
                status = pwd + '/status.txt'
                recipient = q2
                sender = q3
                password = q4
                subject = q7

                if (q6 != 1) and (q6 != 2):  # if the value is not acceptable
                    print("You entered an invalid option for your IP tracking settings, defaulting to local IP option.")
                    ip_type = 1
                else:
                    ip_type = int(q6)

                #  Appends settings to settings list for writing
                settings_list.append(php_config)
                settings_list.append(log_file)
                settings_list.append(ip)
                settings_list.append(recipient)
                settings_list.append(sender)
                settings_list.append(password)
                settings_list.append(subject)
                settings_list.append(server_address)
                settings_list.append(server_port)
                settings_list.append(ip_type)
                mode = '1'

        elif mode == '1':  # manual config mode
            # Options menu
            print("\nWhat would you like to do? \n"
                  "1. Configure Settings\n"
                  "2. Edit Settings\n"
                  "3. Check settings\n"
                  "4. Save settings\n"
                  "5. Finish\n")
            user_choice = input("Enter a number option: ")

            # Configure settings
            if user_choice == '1':
                setting_write = False
                settings_list = []
                php_config = input("What is the path to your NextCloud config.php file? If you do not "
                                   "have one, put 'none': ")
                log_file = input("What is the path to your desired log file location?: ")
                ip = input("What is the path to your desired ip storage? This "
                           "should be in the same folder as ip_tracker.py: ")
                recipient = input("What email should the email be sent to?: ")
                sender = input("What email will be sending the message?: ")
                password = input("What is the password for the sending email?: ")
                subject = input("What should the subject of the email be?: ")
                server_address = input("What is you email providers server address?: ")
                server_port = input("What port number does your email sever use?: ")
                ip_type = input("Would you like your:\n"
                                "1. local IP\n"
                                "2: WAN IP?\n"
                                ": ")
                # Appending settings to setting list for temporary storage
                settings_list.clear()  # clears all settings for clean write
                settings_list.append(php_config)
                settings_list.append(log_file)
                settings_list.append(ip)
                settings_list.append(recipient)
                settings_list.append(sender)
                settings_list.append(password)
                settings_list.append(subject)
                settings_list.append(server_address)
                settings_list.append(server_port)
                settings_list.append(int(ip_type))

            # edit settings
            elif user_choice == '2':
                setting_write = False
                apply = False
                while not apply:
                    print('Which setting would you like to change?\n'
                          '1. php.config file\n'
                          '2. log file\n'
                          '3. wan file\n'
                          '4. recipient email\n'
                          '5. sender email\n'
                          '6. sender password\n'
                          '7. subject\n'
                          '8. email server address\n'
                          '9. email server port\n'
                          '10. ip tracking\n')
                    user_change = input("Please select a number option or type 'done' to exit: ")
                    if user_change == '1':
                        user_rewrite = input("What should the new php.config location be?: ")
                        settings_list.pop(0)
                        settings_list.insert(0, user_rewrite)
                    elif user_change == '2':
                        user_rewrite = input("What should the new log location be?: ")
                        settings_list.pop(1)
                        settings_list.insert(1, user_rewrite)
                    elif user_change == '3':
                        user_rewrite = input("What should the new wan location be?: ")
                        settings_list.pop(2)
                        settings_list.insert(2, user_rewrite)
                    elif user_change == '4':
                        user_rewrite = input("What should the new recipient email be?: ")
                        settings_list.pop(3)
                        settings_list.insert(3, user_rewrite)
                    elif user_change == '5':
                        user_rewrite = input("What should the new sender email be?: ")
                        settings_list.pop(4)
                        settings_list.insert(4, user_rewrite)
                    elif user_change == '6':
                        user_rewrite = input("What should the new sender password be?: ")
                        settings_list.pop(5)
                        settings_list.insert(5, user_rewrite)
                    elif user_change == '7':
                        user_rewrite = input("What should the new subject be?: ")
                        settings_list.pop(6)
                        settings_list.insert(6, user_rewrite)
                    elif user_change == '8':
                        user_rewrite = input("What should the new email server address be?: ")
                        settings_list.pop(7)
                        settings_list.insert(7, user_rewrite)
                    elif user_change == '9':
                        user_rewrite = input("What should the new server port be?: ")
                        settings_list.pop(8)
                        settings_list.insert(8, user_rewrite)
                    elif user_change == '10':
                        user_rewrite = input("Would you like your:\n"
                                             "1. local IP\n"
                                             "2: WAN IP?\n"
                                             ": ")
                        settings_list.pop(9)
                        settings_list.insert(9, user_rewrite)
                    elif user_change.lower() == 'done':
                        break
                    else:
                        print("'" + user_change + "'" + ' is not an option, please try again.')

            # Check settings
            elif user_choice == '3':
                print('The following will be written:' + '\n')
                print('The php file location is: ' + settings_list[0])
                print('The log file location is: ' + settings_list[1])
                print('The old wan file location is: ' + settings_list[2])
                print('The recipient email is: ' + settings_list[3])
                print('The sender email is: ' + settings_list[4])
                print('The sender password is: ' + settings_list[5])
                print('The email email subject is: ' + settings_list[6])
                print('The email server address is: ' + settings_list[7])
                print('The email server port is: ' + str(settings_list[8]))
                print('The ip tracking type is: ' + str(settings_list[9]))

            # Write settings
            elif user_choice == '4':
                setting = open('settings.txt', 'w')  # Settings file to be written to
                for item in settings_list:
                    setting.write(str(item) + '\n')
                print('Settings written.')
                setting_write = True
                setting.close()

            # End program
            elif user_choice == '5':
                if setting_write is True:  # if the settings were written, do this
                    print("Your settings have been saved.")
                    # This section generates the log and wan storage files.
                    if old_settings is False:
                        log = open(log_file, 'w')
                        wan = open(ip, 'w')
                        stat = open(status, 'w')
                        stat.write('0')
                        stat.close()
                        wan.close()
                        log.close()
                    done = True
                else:  # else tell the user that no settings were saved
                    print("Your settings were never saved, to save your settings please write them before closing the "
                          "program.")
                    retry = input("Would you like to go back and save your settings? y/N: ")
                    if retry.lower() == 'y':
                        continue
                    else:
                        done = True


if __name__ == "__main__":
    main()
